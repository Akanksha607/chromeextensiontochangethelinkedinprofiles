// Function to replace profile pictures
function replaceProfilePictures() {
    // The new image URL to replace all profile pictures with
    const newProfilePicUrl = 'https://pics.craiyon.com/2023-06-20/89f79a8dee744596981e7417b8a7ea1d.webp'; // Change this URL to the desired image URL

    // Select all profile picture elements on LinkedIn's feed page
    const profilePictures = document.querySelectorAll('.feed-shared-actor__avatar-image, .ivm-view-attr__img--centered, .update-components-actor__avatar, .profile-rail-card__actor-link img');

    profilePictures.forEach(picture => {
        if (picture.tagName === 'IMG') {
            // Replace the profile picture with the new image URL
            picture.src = newProfilePicUrl;
            picture.srcset = newProfilePicUrl;
        } else {
            // Handle cases where the profile picture might be a background image
            picture.style.backgroundImage = `url(${newProfilePicUrl})`;
        }
    });
}

// Run the function when the content script is loaded
replaceProfilePictures();

// Optional: MutationObserver to handle dynamically loaded content
const observer = new MutationObserver((mutations) => {
    mutations.forEach(() => {
        replaceProfilePictures();
    });
});

observer.observe(document.body, { childList: true, subtree: true });
